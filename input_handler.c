#include <dirent.h>
#include <fcntl.h>
#include <linux/input.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifndef NO_INOTIFY
#include <sys/inotify.h>
#endif

#ifndef NO_SGESTURES
#include <sgestures/touch.h>
#include <sgestures/writer.h>
#endif

#define LEN(A) sizeof(A)/sizeof(A[0])
#define BITS_PER_LONG (sizeof(long) * 8)
#define NBITS(x) ((((x)-1)/BITS_PER_LONG)+1)
#define OFF(x)  ((x)%BITS_PER_LONG)
#define BIT(x)  (1UL<<OFF(x))
#define LONG(x) ((x)/BITS_PER_LONG)
#define test_bit(bit, array)	((array[LONG(bit)] >> OFF(bit)) & 1)

#define WILDCARD (-2)
#define MAX_DEVICES 255
#define MAX_NAME_LEN 64
#define MAX_SLOTS 32
#define DEVINPUT_PATH "/dev/input"

#define INFO(...) dprintf(STDERR_FILENO, __VA_ARGS__);

void die(const char*msg) {
    perror(msg);
    exit(2);
}

typedef struct {
    unsigned char type;
    unsigned int code;
    unsigned int value;
    void (*func)();
    const char* cmd;
} Binding;

typedef enum {
    INACTIVE,
    STARTED,
    CONTINUING,
    FINISHED,
    CANCELED,
} SlotState;

typedef struct {
    unsigned int abs[2];
    SlotState state : 3;
    char dirty : 1;
} Slot;
typedef struct {
    int min;
    int max;
} Range;

typedef struct {
    int id;
    char devname[MAX_NAME_LEN];
    char devnameLen;
    char name[MAX_NAME_LEN];
    char nameLen;
    Slot slots[MAX_SLOTS];
    Range ranges[2];
    unsigned char activeSlot;
} Device;

void spawn(Binding*binding) {
    system(binding->cmd);
}
void setX(Binding*binding, Device* device, int value) {
    device->slots[device->activeSlot].abs[0] = value;
    device->slots[device->activeSlot].dirty = 1;
}
void setY(Binding*binding, Device* device, int value) {
    device->slots[device->activeSlot].abs[1] = value;
    device->slots[device->activeSlot].dirty = 1;
}
void setActiveSlot(Binding*binding, Device* device, int value) {
    device->activeSlot = value;
}
void enableSlot(Binding*binding, Device* device, int value) {
    device->slots[device->activeSlot].state = value != -1 ? STARTED : FINISHED;
    device->slots[device->activeSlot].dirty = 1;
}
#ifndef NO_SGESTURES
void sendInput(Binding*binding, Device* device, int value, long time) {
    if(!device->slots[device->activeSlot].dirty)
        return;
    device->slots[device->activeSlot].dirty;
    GestureMask mask = 0;
    INFO("%d %d %d ",mask, device->slots[device->activeSlot].state, device->activeSlot);
    switch(device->slots[device->activeSlot].state) {
        case CONTINUING:
            mask = TouchMotionMask;
            INFO("CONTINUING");
            break;
        case FINISHED:
            INFO("FINISHED");
            mask = TouchEndMask;
            device->slots[device->activeSlot].state = INACTIVE;
            break;
        case CANCELED:
            INFO("CANCELED");
            mask =  TouchCancelMask;
            device->slots[device->activeSlot].state = INACTIVE;
            break;
        case STARTED :
            INFO("STARTED");
            mask = TouchStartMask;
            device->slots[device->activeSlot].state = CONTINUING;
            break;
        case INACTIVE:
        default:
            die("Invalid state");
    }
    INFO("\n");

    LargestRawGestureEvent event = {{mask,
        {
            device->id,
            device->activeSlot,
            { device->slots[device->activeSlot].abs[0] , device->slots[device->activeSlot].abs[1]},
            {
                100 * (device->slots[device->activeSlot].abs[0] - device->ranges[0].min) / device->ranges[0].max,
                100 * (device->slots[device->activeSlot].abs[1] - device->ranges[1].min) / device->ranges[1].max,
            } ,
            time
        }
    }};
    if(mask == TouchStartMask) {
        setRawGestureEventNames(&event, device->name, device->devname);
    }
    writeTouchEvent(STDOUT_FILENO, &event.event);

}
#endif

Device devices[MAX_DEVICES];
struct pollfd fds[MAX_DEVICES];
char grab;
int numDevices;
int inotifyFd = -1;

void setDevice(int fd, int index, const char* path) {
	unsigned short id[4];

	ioctl(fd, EVIOCGID, id);
    // Use ID_BUS ID_VENDOR for id
    devices[index].id = *((int*)id);
	ioctl(fd, EVIOCGNAME(MAX_NAME_LEN - 1), devices[index].name);
    devices[index].nameLen =  strlen(devices[index].name);

    strncpy(devices[index].devname, path, MAX_NAME_LEN - 1);
    devices[index].devnameLen =  strlen(devices[index].devname);

    INFO("Listening on %s %s\n", path, devices[index].name);

    // value, min, max, fuzz, flat, resolution
	int abs[6] = {0};
    int codes[] = {ABS_X, ABS_Y};
    ioctl(fd, EVIOCGABS(ABS_MT_SLOT), abs);
    setActiveSlot(NULL, devices + index, abs[0]);
    for(int i = 0; i < LEN(codes); i++) {
        ioctl(fd, EVIOCGABS(codes[i]), abs);
        devices[index].slots[devices[index].activeSlot].abs[i] = abs[0];
        devices[index].ranges[i].min = abs[1];
        devices[index].ranges[i].max = abs[2];
    }
}

Binding bindings[] = {
    {EV_SW, SW_LID, 1, spawn, "/usr/bin/suspend"},
    {EV_ABS, ABS_MT_TRACKING_ID, WILDCARD, enableSlot},
    {0},
#ifndef NO_SGESTURES
    {EV_SYN, WILDCARD, WILDCARD, sendInput},
    {EV_ABS, ABS_X, WILDCARD, setX},
    {EV_ABS, ABS_Y, WILDCARD, setY},
    {EV_ABS, ABS_MT_SLOT, WILDCARD, setActiveSlot},

#endif
};

struct {
    unsigned int type;
    unsigned int code;
} excludedDevices[] ={
    {EV_KEY, BTN_TOOL_FINGER},
};
static int has_relevant_input(int fd)
{
	unsigned long bit[EV_MAX][NBITS(KEY_MAX)] = {0};
	ioctl(fd, EVIOCGBIT(0, EV_MAX), bit[0]);

    for(int i = 0 ; i < LEN(excludedDevices);i++) {
        if(test_bit(excludedDevices[i].type, bit[0])){
			ioctl(fd, EVIOCGBIT(excludedDevices[i].type, KEY_MAX), bit[excludedDevices[i].type]);
            if(excludedDevices[i].code == WILDCARD || test_bit(excludedDevices[i].code, bit[excludedDevices[i].type]))
                return 0;
        }
    }

    for(int i = 0 ; i < LEN(bindings) && bindings[i].type;i++) {
        if(test_bit(bindings[i].type, bit[0])){
			ioctl(fd, EVIOCGBIT(bindings[i].type, KEY_MAX), bit[bindings[i].type]);
            if(bindings[i].code == WILDCARD || test_bit(bindings[i].code, bit[bindings[i].type]))
                return 1;
        }
    }
	return 0;
}

void processEvent(const Device* device, const struct input_event* event) {
    for(int i = 0 ; i < LEN(bindings);i++) {
        if(bindings[i].func &&
                (bindings[i].type == WILDCARD || bindings[i].type == event->type) &&
                (bindings[i].code == WILDCARD || bindings[i].code == event->code) &&
                (bindings[i].value == WILDCARD || bindings[i].value == event->value)) {
            bindings[i].func(&bindings[i], device, event->value, event->time.tv_sec * 1000 + event->time.tv_usec / 1000);
            break;
        }
    }
}

int addDevice(const char*path, int numFds) {
    if(numDevices >= MAX_DEVICES)
        return 0;

    int fd;
    if ((fd = open(path, O_RDONLY | O_NONBLOCK | O_CLOEXEC)) < 0) {
        die("Could not open filename");
    }
    if(grab)
        if(!ioctl(fd, EVIOCGRAB, (void*)1))
            die("Could not grab device");
    if(!has_relevant_input(fd))
        return 0;
    char name[MAX_NAME_LEN] = "Unknown";
    fds[numFds] = (struct pollfd){fd, POLLIN };
    setDevice(fd, numFds, path);
    return 1;
}

#ifndef NO_INOTIFY
int handle_inotify(int fd, int numFds) {
    static char buffer[sizeof(struct inotify_event) + 255];
    struct inotify_event* event = (void*)buffer;
    int ret = read(fd, event, sizeof(buffer));
    if(ret == -1) {
        perror("Failed to read");
        return 0;
    }
    //inotify
    return addDevice(event->name, numFds);
}
#endif

void run(struct pollfd* fds, int numFds) {
	struct input_event ev;
    int rd;
    while(numFds) {
        int ret = poll(fds, numFds, -1);
        if(ret == -1)
            break;

        for(int i = numFds - 1; i>=0; i--) {
            if(fds[i].revents & (POLLERR | POLLHUP)) {
                INFO("Removing device %s %s index %d", devices[i].name, devices[i].devname, i);
                memcpy(fds + i, fds + i + 1, sizeof(struct pollfd) * (numFds - i - 1));
                memcpy(devices + i, devices + i + 1, sizeof(Device) * (numFds - i - 1));
                numFds--;
                continue;
            }
            if(fds[i].revents & (POLLIN)) {

#ifndef NO_INOTIFY
                if(fds[i].fd == inotifyFd) {
                    numFds += handle_inotify(inotifyFd, numFds);
                    continue;
                }
#endif
                rd = read(fds[i].fd, &ev, sizeof(ev));
                if (rd < (int) sizeof(struct input_event)) {
                    perror("\nevtest: error reading");
                    return;
                }
                processEvent(devices + i, &ev);
            }
        }
    }
}

int addDir(const char* path, int numFds) {
    int fd = open(path, O_RDONLY );
    DIR* d = fdopendir(fd);
    if(!d)
        return 0;
    struct dirent * dir;
    while ((dir = readdir(d)) != NULL) {
        if(dir->d_name[0] == '.')
            continue;
        if(dir->d_type != DT_DIR)
            numFds += addDevice(dir->d_name, numFds);
    }
    closedir(d);
    return numFds;
}

int main(int argc, char* argv[]) {
    int numFds = 0;
    if(argv[1] && argv[1][0] == '-' && argv[1][1] == 'g' && !argv[1][2] ) {
        argv++;
        grab = 1;
    }
    const char** filenames = ++argv;
#ifndef NO_INOTIFY
    if(!*filenames) {
        inotifyFd = inotify_init1(IN_NONBLOCK | IN_CLOEXEC);
        chdir(DEVINPUT_PATH);
        inotify_add_watch(inotifyFd , DEVINPUT_PATH, IN_CREATE);
        fds[0] = (struct pollfd){inotifyFd, POLLIN };
        numFds = addDir(DEVINPUT_PATH, 1);
    }
#endif
    for(int i = 0; *filenames && i < MAX_DEVICES; i++, filenames++) {
        numFds += addDevice(*filenames, numFds);
    }
    run(fds, numFds );
}
