PREFIX ?= /usr

NO_INOTIFY ?= 0
NO_INOTIFY_FLAG_1 = -DNO_INOTIFY
NO_SGESTURES ?= 0
NO_SGESTURES_FLAG_1 = -DNO_SGESTURES

CPPFLAGS += $(NO_SGESTURES_FLAG_$(NO_SGESTURES)) $(NO_INOTIFY_FLAG_$(NO_INOTIFY))

all: input_handler

install: input_handler
	install -Dt $(DESTDIR)$(PREFIX)/bin input_handler

clean:
	rm input_handler
